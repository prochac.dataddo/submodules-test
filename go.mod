module gitlab.com/prochac.dataddo/submodules-test

go 1.20

require (
	gitlab.com/prochac.dataddo/pseudo-commons v0.0.0-20230720113827-a7186734ae7c
	gitlab.com/prochac.dataddo/pseudo-commons/document v0.0.0-20230720113827-a7186734ae7c
	gitlab.com/prochac.dataddo/pseudo-commons/grpcclient v0.0.0-20230720113827-a7186734ae7c
	gitlab.com/prochac.dataddo/pseudo-commons/pgq v0.0.0-20230720113827-a7186734ae7c
	gitlab.com/prochac.dataddo/pseudo-grpc-repo/gen/proto/go v0.0.0-20230720112714-bc89ba15b04a
)
