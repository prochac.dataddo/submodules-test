package main

import (
	_ "gitlab.com/prochac.dataddo/pseudo-commons/document"
	_ "gitlab.com/prochac.dataddo/pseudo-commons/grpcclient"
	"gitlab.com/prochac.dataddo/pseudo-commons/musthaveshit"
	"gitlab.com/prochac.dataddo/pseudo-commons/pgq"
	"gitlab.com/prochac.dataddo/pseudo-grpc-repo/gen/proto/go"
)

func main() {
	_ = musthaveshit.EveryWantsThis
	_ = pgq.SomeWantThis
	_ = grpc.SomeGRPC
}
